import React, { Component } from 'react';
import _ from 'lodash';
import './App.css';
import NULLmetro from './public/images/null/metro-icon.png';
import RDmetro from './public/images/RD/metro-icon.png';
import BLmetro from './public/images/BL/metro-icon.png';
import GRmetro from './public/images/GR/metro-icon.png';
import ORmetro from './public/images/OR/metro-icon.png';
import SVmetro from './public/images/SV/metro-icon.png';
import YLmetro from './public/images/YL/metro-icon.png';
import NULLcar from './public/images/null/singlecar.png';
import RDcar from './public/images/RD/singlecar.png';
import BLcar from './public/images/BL/singlecar.png';
import GRcar from './public/images/GR/singlecar.png';
import ORcar from './public/images/OR/singlecar.png';
import SVcar from './public/images/SV/singlecar.png';
import YLcar from './public/images/YL/singlecar.png';


function TrainList(props) {
    const trainPositions = props.trainPositions;
    if (trainPositions == null)
    {
        return (
            <h2 className="noResults">
              NO RESULTS FOUND
            </h2>
        );
    }
    const trainListItems = trainPositions.map((trainPosition) =>
                                              <li className="list-element">
                                                <ListItemComponent
                                                  trainId={trainPosition.TrainId}
                                                  serviceType ={trainPosition.ServiceType}
                                                  directionNum={trainPosition.DirectionNum}
                                                  lineCode={trainPosition.LineCode}
                                                  carCount={trainPosition.CarCount}
                                                  trainNumber ={trainPosition.TrainNumber}
                                                  destinationStationCode={trainPosition.DestinationStationCode}
                                                  secondsAtLocation={trainPosition.SecondsAtLocation}
                                                  circuitId={trainPosition.CircuitId} />
                                               </li>
                                            );
    return (
        <ul>{trainListItems}</ul>
    );
}

class ListItemComponent extends Component {
    render() {
        const icons = {null: NULLmetro, "RD": RDmetro, "BL": BLmetro, "GR": GRmetro, "OR": ORmetro, "SV": SVmetro, "YL": YLmetro};
        const singlecarIcons = {null: NULLcar, "RD": RDcar, "BL": BLcar, "GR": GRcar, "OR": ORcar, "SV": SVcar, "YL": YLcar};
        const imgUrl = icons[this.props.lineCode];
        const singlecarImgUrl = singlecarIcons[this.props.lineCode];
        let carImageLinks = new Array(parseInt(this.props.carCount));
        for (let index = 0; index < this.props.carCount; index++)
        {
            carImageLinks[index] = singlecarImgUrl;
        }
        const carImageList = carImageLinks.map((carImageLink) =>
                                               <li className="singlecar-list-element">
                                                 <img className="singlecar-image" src={carImageLink}/>
                                               </li>
                                          );
        
        return (
            <div className="list-item">
              <img className="list-item-image" src={imgUrl} />
              <span className="list-item-span">
                <div>
                  <ul className="car-img-list">{carImageList}</ul>
                </div>
                <div className="item-details">
                  <span>
                    <h4>TRAIN ID : {this.props.trainId}</h4>
                    <h4>TRAIN NUMBER : {this.props.trainNumber}</h4>
                    <h4>CIRCUIT ID : {this.props.circuitId}</h4>
                    <h4>NUMBER OF CARS : {this.props.carCount}</h4>
                    <h4>DESTINATION STATION CODE : {this.props.destinationStationCode}</h4>
                  </span>
                  <span>
                    <h4><span className="serviceType-holder">{this.props.serviceType} </span></h4>
                    <h4>SECONDS AT LOCATION : {this.props.secondsAtLocation}</h4>
                    <h4>DIRECTION NUM : {this.props.directionNum}</h4>
                    <h4>LINE CODE : {this.props.lineCode}</h4>
                  </span>                  
                </div>
              </span>
            </div>
        );
    }
}


class App extends Component {

    constructor(props) {
	super(props);
	this.state = {
	    trains: {},
            filteredTrainPositions: [],
            trainPositions: []
	};
    }

    componentDidMount() {
	this.fetchData();
        // setInterval(this.fetchData, 2000);
         
    }

    fetchData()
    {
	fetch("https://api.wmata.com/TrainPositions/TrainPositions?contentType=json&api_key=572d0588c57d409a9642c28879b9b208")
	    .then(response => response.json())
	    .then(data => {
		this.setState({
                    trains : data,
                    trainPositions: data.TrainPositions,
                    filteredTrainPositions: data.TrainPositions
                });
	    });
    }

    handleclick(filter, filterValue) {
        let filteredData = null;

        if (filter)
        {
            filteredData = _.groupBy(this.state.trainPositions, filter);
            filteredData = filteredData[filterValue]
        }
        else filteredData = this.state.trainPositions;

        this.setState({
            filteredTrainPositions : filteredData
        });
        
    }
    
    render() {
        const trains = this.state.trains;
        const trainPositions = this.state.filteredTrainPositions;
               
	return (
	    <div>
              <div className="filter-banner">
                <div>
                  <h2>Filters : </h2>
                </div>
                <div>
                  <span>Color : </span>
                  <ul className = "filter-button-list color-list">
                    <li><button className="redButton" onClick={() => this.handleclick('LineCode','RD')}>RED</button></li>
                    <li><button className="blueButton" onClick={() => this.handleclick('LineCode','BL')}>BLUE</button></li>
                    <li><button className="greenButton" onClick={() => this.handleclick('LineCode','GR')}>GREEN</button></li>
                    <li><button className="orangeButton" onClick={() => this.handleclick('LineCode','OR')}>ORANGE</button></li>
                    <li><button className="yellowButton" onClick={() => this.handleclick('LineCode','YL')}>YELLOW</button></li>
                    <li><button className="silverButton" onClick={() => this.handleclick('LineCode','SV')}>SILVER</button></li>
                    <li><button className="nullButton" onClick={() => this.handleclick('LineCode','null')}>NULL</button></li>
                  </ul>
                </div>
                <div>
                  <span>Service Type : </span>
                  <ul className = "filter-button-list serviceType-list">
                    <li><button onClick={() => this.handleclick('ServiceType','NoPassengers')}>NoPassengers</button></li>
                    <li><button onClick={() => this.handleclick('ServiceType','Normal')}>Normal</button></li>
                    <li><button onClick={() => this.handleclick('ServiceType','Special')}>Special</button></li>
                    <li><button onClick={() => this.handleclick('ServiceType','Unknown')}>Unknown</button></li>
                  </ul>
                </div>
                <div>
                  <span>Car Count : </span>
                  <ul  className = "filter-button-list carCount-list">
                    <li><button onClick={() => this.handleclick('CarCount','0')}>0</button></li>
                    <li><button onClick={() => this.handleclick('CarCount','2')}>2</button></li>
                    <li><button onClick={() => this.handleclick('CarCount','6')}>6</button></li>
                    <li><button onClick={() => this.handleclick('CarCount','8')}>8</button></li>
                  </ul>
                </div>
                
                <div className="all-trains">
                  <button onClick={() => this.handleclick(null,null)}>Show all trains</button>
                </div>
              </div>
              <div>
                <TrainList trainPositions={trainPositions} />
              </div>
              <div>
                
              </div>
          
            </div>
	);
    }
}

export default App;
